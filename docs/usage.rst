=====
Usage
=====

To use pyfb-reporting in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_reporting.apps.PyfbReportingConfig',
        ...
    )

Add pyfb-reporting's URL patterns:

.. code-block:: python

    from pyfb_reporting import urls as pyfb_reporting_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_reporting_urls)),
        ...
    ]
