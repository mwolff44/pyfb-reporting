============
Installation
============

At the command line::

    $ easy_install pyfb-reporting

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-reporting
    $ pip install pyfb-reporting
