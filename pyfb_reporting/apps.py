# -*- coding: utf-8
from django.apps import AppConfig


class PyfbReportingConfig(AppConfig):
    name = 'pyfb_reporting'
