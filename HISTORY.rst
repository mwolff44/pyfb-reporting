.. :changelog:

History
-------

1.2.1 (2020-10-11)
++++++++++++++++++

* Correct cdr detailed view (billsec and costsec were inversed)

1.2.0 (2020-10-06)
++++++++++++++++++

* Update API
* Add field in CDR to have the correct value of the billed duration by provider

1.1.2 (2020-05-29)
++++++++++++++++++

* Correct little bug

1.1.0 (2020-05-27)
++++++++++++++++++

* Add a link to gross CDRs in CDR detail view
* Add CDR export feature
* New date filter in CDR list

1.0.0 (2020-05-21)
++++++++++++++++++

* Provider can be search in CDR list view
* CDR detail view has been reworked
* Call class has been added (on-net / off-net) 

0.9.5 (2020-02-21)
++++++++++++++++++

* Error in DB migration 

0.9.4 (2020-02-19)
++++++++++++++++++

* Add uuid field for CDR 
* Create RREvent model

0.9.3 (2019-06-23)
++++++++++++++++++

* Show CDR form Latest one to older ones

0.9.2 (2019-06-22)
++++++++++++++++++

* Add route_json in cdr
* Remove unused field
* Admin interface enhancements

0.9.1 (2019-06-18)
++++++++++++++++++

* Change duration filed from integer to decimal.

0.9.0 (2019-04-30)
++++++++++++++++++

* First release on PyPI.
